**The problem : on a dual-boot system, after installing CentOS, Debian doesn't recognize the swap partition anymore because its UUID has been changed. Here's how to solve this.**

In Debian:

- The new UUID must be written in `/etc/fstab`
- It also must be written in the initramfs `resume` config file :

```
sudo echo "RESUME=UUID=new_uuid" | sudo tee /etc/initramfs-tools/conf.d/resume
sudo update-initramfs -u
```
- The swap partition must be reset via `mkswap /dev/sdX`
- It must be enabled via `swapon -a`

Et voilà!